import org.junit.Test;
import static org.junit.Assert.*;

public class DoubleLinkedListTest {
    
    DoubleLinkedList list = new DoubleLinkedList();

    @Test
    public void doesAllWorkPerfectly() {
        DoubleLinkedList list = new DoubleLinkedList();        
        list.add("fred");
        list.add("wilma");
        list.add("barney");
        list.delete(list.find("fred").getValue());
        assertArrayEquals(new String[] {"wilma","barney"}, list.values());
        list.delete(list.find("wilma").getValue());
        assertArrayEquals(new String[] {"barney"}, list.values());
        list.delete(list.find("barney").getValue());
        assertArrayEquals(new String[] {}, list.values());
    }

    @Test
    public void doesFindCorrectWord() {
        DoubleLinkedList list = new DoubleLinkedList();
        list.add("Franek");
        list.add("Lolek");
        list.add("Bolek");
        assertNotNull(list.find("Bolek"));
        assertEquals("Bolek", list.find("Bolek").getValue());
    }
    
    @Test
    public void doesMakeCorrectlyCheckingOfNodesExisting() {
        DoubleLinkedList list = new DoubleLinkedList();
        list.add("Franek");
        list.add("Lolek");
        list.add("Bolek");
        assertTrue(list.doesExistNodeByString("Bolek"));
    }
}
