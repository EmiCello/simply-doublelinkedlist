import org.junit.Test;

import static org.junit.Assert.*;

public class SimplyListTest {
    
    Lista lista = new Lista();  
    
    @Test
    public void allWorkPerfectly() {
        Lista list = new Lista();
        list.add("fred");
        list.add("wilma");
        list.add("betty");
        list.add("barney");
        assertArrayEquals(new String[] {"fred", "wilma", "betty", "barney"}, list.values());
        list.delete(list.find("wilma").getValue());
        assertArrayEquals(new String[] {"fred", "betty", "barney"}, list.values());
        list.delete(list.find("barney").getValue());
        assertArrayEquals(new String[] {"fred", "betty"}, list.values());
        list.delete(list.find("fred").getValue());
        assertArrayEquals(new String[] {"betty"}, list.values());
        list.delete(list.find("betty").getValue());
        assertArrayEquals(new String[] {}, list.values());
    }
    
    @Test
    public void doesMakeCorrectArrayOfValues(){
        Lista list = new Lista();
//        list.add("franek");
//        list.add("lucy");
//        list.add("tola");
//        list.add("pola");
//        list.add("lucy");
        assertArrayEquals(new String[] {}, list.values());
        
    }
    
    @Test
    public void doesFindCorrectly() {
        Lista list = new Lista();
        list.add("ala");
        list.add("beka");
        assertEquals("beka", list.find("beka").getValue());
    }
    
    @Test
    public void doesMakeCorrectTabOfNodes() {
        Lista list = new Lista();
        list.add("ala");
        list.add("beka");
        list.add("coco");
        assertArrayEquals(new String[] {"ala", "beka", "coco"}, list.values());
    }
        
    @Test
    public void doesDeleteCorrectly() {
        Lista list = new Lista();
        list.add("ala");
        list.add("beka");
        list.add("coco");
        list.add("didi");
        list.add("efe");
        String wordToDelete = "efe";
        list.delete(list.find(wordToDelete).getValue());
        assertFalse(list.doesExistNodeByString(wordToDelete));
    }
    
    @Test
    public void doesMakeCorrectChecking() {
        Lista list = new Lista();
        list.add("ala");
        list.add("beka");
        list.add("coco");
        list.add("didi");
        list.add("efe");
        assertTrue(list.doesExistNodeByString("beka"));
    }
    
    @Test
    public void addsCorrect() {
        Lista list = new Lista("Flora"); 
        assertEquals("Flora", list.find("Flora").getValue());       
    }
    
    @Test
    public void doesFindCorrectWord() {
        Lista list = new Lista();
        list.add("Franek");
        list.add("Lolek");
        list.add("Bolek");
        assertNotNull(list.find("Bolek"));
    }
}
