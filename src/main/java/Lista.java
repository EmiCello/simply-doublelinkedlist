import java.util.Arrays;

public class Lista {

    private NodeOfSinglyList head;    
    
    public Lista(String value) {
        this.head = new NodeOfSinglyList(value);
    }  
    
    public Lista() {
        this.head = null;
    }
    
    public void add(String s) {
        NodeOfSinglyList newNode = new NodeOfSinglyList(s);
        if (this.head == null) {
            this.head = newNode;
        } else {
            NodeOfSinglyList current = this.head;
            while(current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(newNode);
        }
    }
    
    public boolean doesExistNodeByString(String s) {       
        NodeOfSinglyList tmpNode = find(s);
        return tmpNode != null ? true : false;
    }    
    
    public NodeOfSinglyList find(String s) {
        NodeOfSinglyList current = this.head;
        while (current != null) {
            if(current.getValue().equals(s)){
                return current;                
            }            
            current = current.getNext();            
        }
        return null;
    }

    public boolean delete(String s) {
        NodeOfSinglyList current = this.head;
        NodeOfSinglyList previous = null;
        
        while (current != null) {
            if (current.getValue().equals(s)) {
                if (previous != null) {
                    previous.setNext(current.getNext());
                    current = current.getNext();
                } else {
                    current = current.getNext();
                    head = current;
                }
                return true;               
            } else {
                if (current.getNext() != null) {
                    previous = current;
                    current = current.getNext();
                } else {
                    current = current.getNext();
                }
            }
        }
        return false;
    }
    
    public String[] values() {
        String[] tabOfAllValues = new String[] {};
        NodeOfSinglyList current = this.head; 
        
        int i = 0;
        while (current != null) { 
            tabOfAllValues = Arrays.copyOf(tabOfAllValues, tabOfAllValues.length + 1);
            tabOfAllValues[i] = current.getValue();
            current = current.getNext();
            i++;
        }
        return tabOfAllValues;
   }
}
