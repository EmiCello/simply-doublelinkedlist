import java.util.Arrays;

public class DoubleLinkedList {
    
    private Node head;
    private Node tail;

    public DoubleLinkedList(String value) {
        this.head = new Node(value, null, null);
        this.tail = this.head;
    }

    public DoubleLinkedList() {
        this.head = null;
        this.tail = null;
    }

    public void add(String s) {
        Node node = new Node(s, null, null);
        if (this.head == null) {
            this.head = node;
            this.tail = head;
        } else {
            node.setPrevious(this.tail);
            this.tail.setNext(node);
            this.tail = node;
        }
    }

    public void delete(String s) {
        Node current = this.head;
        
        while (current != null) {
            if (current.getValue().equals(s)) {
                if (current == this.head) {
                    this.head = (current.getNext());                    
                    this.tail = (current.getPrevious());
                    break;
                } else if (current == this.tail) {
                    this.tail = (current.getPrevious());
                    this.tail.setNext(null);
                    break;
                } else {
                    (current.getPrevious()).setNext(current.getNext());
                    current.setNext(null);
                    break;
                }
            }
            current = current.getNext();            
        }
    }


    public Node find(String s) {
        Node current = this.head;
        while (current != null) {
            String tmp = current.getValue();
            if(tmp.equals(s)){
                return current;                
            }
            current = current.getNext();
        }
        return null;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public String[] values() {
        String[] tabOfAllValues = new String[] {};
        Node current = this.head;

        int i = 0;
        while (current != null) {
            tabOfAllValues = Arrays.copyOf(tabOfAllValues, tabOfAllValues.length + 1);
            tabOfAllValues[i] = current.getValue();
            current = current.getNext();
            i++;
        }
        return tabOfAllValues;
    }

    public boolean doesExistNodeByString(String s) {
        Node tmpNode = find(s);
        return tmpNode != null ? true : false;
    }

}
