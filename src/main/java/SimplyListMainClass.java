
public class SimplyListMainClass {
    
    public static void main(String[] args) {
        
        Lista list = new Lista();
        list.add("fred");
        list.add("wilma");        
        list.add("barney");
        list.add("tola");
        list.add("bolek");
        list.add("tola");
        String[] values = list.values(); 
        for(String s : values) {
            System.out.println(s);
        }

        System.out.println("-------------------");
        System.out.println(list.doesExistNodeByString("ala"));
        System.out.println("--------------------");
        
        list.delete(list.find("tola").getValue());
        String[] strings = list.values();
        for(String s : strings) {
            System.out.println(s);
       }  
    } 
    
}