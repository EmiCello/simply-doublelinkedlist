public class DoubleLinkedListMainClass {
    
    public static void main(String[] args) {
        
        DoubleLinkedList list = new DoubleLinkedList();        
        list.add("Franek");
        list.add("Bolek");
        list.add("lola");
        list.add("florek");
        list.delete(list.find("florek").getValue());
        String[] tab = list.values();        
        for (String s : tab) {
            System.out.println(s);
        }

        System.out.println(XORMethod(false, false));
    }
    
    public static boolean XORMethod(boolean a, boolean b){
        return a ^ b ? true : false;        
    }
}
