public class NodeOfSinglyList{    

    private String value;
    private NodeOfSinglyList next;   
    
    public NodeOfSinglyList(String value) { 
        this.value = value;
        this.next = null;
    }

    public String getValue(){ 
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }    
    public NodeOfSinglyList getNext() {        
        return next;
    }
    public void setNext(NodeOfSinglyList next) {
        this.next = next;
    }
    
}
